package com.leoalarcon.bibliotheque.controllers;

import com.leoalarcon.bibliotheque.data.repositories.BookRepository;
import com.leoalarcon.bibliotheque.exception.ResourceNotFoundException;
import com.leoalarcon.bibliotheque.model.Book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/book")
public class BookController {

    private final String MSG_NOT_FOUND = "Livre non trouvé ayant l'identifiant ";

    @Autowired
    private BookRepository bookRepository;


    @GetMapping("/")
    public Page<Book> getBooks(Pageable pageable){
        return bookRepository.findAll(pageable);
    }

    @PostMapping("/")
    public Book createBook(@Valid @RequestBody Book book){
        return bookRepository.save(book);
    }

    @PutMapping("/{bookId}")
    public Book updateBook(@PathVariable Long bookId, @Valid @RequestBody Book bookRequest) {
        return bookRepository.findById(bookId)
                .map(book -> {
                    book.setTitle(bookRequest.getTitle());
                    book.setAuthor(bookRequest.getAuthor());
                    book.setIsbn(bookRequest.getIsbn());
                    return bookRepository.save(book);
                }).orElseThrow(() -> new ResourceNotFoundException(MSG_NOT_FOUND + bookId));
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
        return bookRepository.findById(bookId)
                .map(book -> {
                    bookRepository.delete(book);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException(MSG_NOT_FOUND + bookId));
    }


    @DeleteMapping("/")
    public ResponseEntity<?> deleteAll(Pageable pageable) {
       bookRepository.deleteAll();
       return ResponseEntity.ok().build();
    }

}
